From: Ariel D'Alessandro <ariel.dalessandro@collabora.com>
Date: Wed, 31 Aug 2022 12:01:23 -0300
Subject: ARM: dts: Add Raspberry Pi Compute Module 4 CANOPi Board

The Eclipse KUKSA CANOPi [0] is a baseboard for the Raspberry Compute
Module 4 (CM4). It contains a VIA VL805 4 Port USB controller and two
MCP251xFD based CAN-FD interfaces.

[0] https://github.com/boschresearch/kuksa.hardware

Signed-off-by: Ariel D'Alessandro <ariel.dalessandro@collabora.com>
---
 arch/arm/boot/dts/Makefile                         |   1 +
 arch/arm/boot/dts/bcm2711-rpi-cm4-canopi.dts       | 146 +++++++++++++++++++++
 arch/arm64/boot/dts/broadcom/Makefile              |   1 +
 .../boot/dts/broadcom/bcm2711-rpi-cm4-canopi.dts   |   2 +
 4 files changed, 150 insertions(+)
 create mode 100644 arch/arm/boot/dts/bcm2711-rpi-cm4-canopi.dts
 create mode 100644 arch/arm64/boot/dts/broadcom/bcm2711-rpi-cm4-canopi.dts

diff --git a/arch/arm/boot/dts/Makefile b/arch/arm/boot/dts/Makefile
index 05d8aef..8930ab2 100644
--- a/arch/arm/boot/dts/Makefile
+++ b/arch/arm/boot/dts/Makefile
@@ -98,6 +98,7 @@ dtb-$(CONFIG_ARCH_BCM2835) += \
 	bcm2837-rpi-zero-2-w.dtb \
 	bcm2711-rpi-400.dtb \
 	bcm2711-rpi-4-b.dtb \
+	bcm2711-rpi-cm4-canopi.dtb \
 	bcm2711-rpi-cm4-io.dtb \
 	bcm2835-rpi-zero.dtb \
 	bcm2835-rpi-zero-w.dtb
diff --git a/arch/arm/boot/dts/bcm2711-rpi-cm4-canopi.dts b/arch/arm/boot/dts/bcm2711-rpi-cm4-canopi.dts
new file mode 100644
index 0000000..df32f25
--- /dev/null
+++ b/arch/arm/boot/dts/bcm2711-rpi-cm4-canopi.dts
@@ -0,0 +1,146 @@
+// SPDX-License-Identifier: GPL-2.0
+/dts-v1/;
+#include "bcm2711-rpi-cm4.dtsi"
+
+/ {
+	model = "Raspberry Pi Compute Module 4 CANOPi Board";
+
+	clocks {
+		clk_mcp251xfd_osc: mcp251xfd-osc {
+			#clock-cells = <0>;
+			compatible = "fixed-clock";
+			clock-frequency = <20000000>;
+		};
+	};
+
+	leds {
+		led-act {
+			gpios = <&gpio 42 GPIO_ACTIVE_HIGH>;
+		};
+
+		led-pwr {
+			label = "PWR";
+			gpios = <&expgpio 2 GPIO_ACTIVE_LOW>;
+			default-state = "keep";
+			linux,default-trigger = "default-on";
+		};
+	};
+};
+
+&ddc0 {
+	status = "okay";
+};
+
+&ddc1 {
+	status = "okay";
+};
+
+&hdmi0 {
+	status = "okay";
+};
+
+&hdmi1 {
+	status = "okay";
+};
+
+&i2c0 {
+	pinctrl-names = "default";
+	pinctrl-0 = <&i2c0_gpio44>;
+	status = "okay";
+	clock-frequency = <100000>;
+
+	pcf85063a@51 {
+		compatible = "nxp,pcf85063a";
+		reg = <0x51>;
+	};
+};
+
+&pcie0 {
+	pci@0,0 {
+		device_type = "pci";
+		#address-cells = <3>;
+		#size-cells = <2>;
+		ranges;
+
+		reg = <0 0 0 0 0>;
+
+		usb@0,0 {
+			reg = <0 0 0 0 0>;
+			resets = <&reset RASPBERRYPI_FIRMWARE_RESET_ID_USB>;
+		};
+	};
+};
+
+&pixelvalve0 {
+	status = "okay";
+};
+
+&pixelvalve1 {
+	status = "okay";
+};
+
+&pixelvalve2 {
+	status = "okay";
+};
+
+&pixelvalve4 {
+	status = "okay";
+};
+
+&spi {
+	status = "okay";
+	pinctrl-names = "default";
+	pinctrl-0 = <&spi0_gpio7>;
+	cs-gpios = <&gpio 8 1>, <&gpio 7 1>;
+	dmas = <&dma 6>, <&dma 7>;
+	dma-names = "tx", "rx";
+
+	mcp251xfd0: mcp251xfd@0 {
+		compatible = "microchip,mcp251xfd";
+		reg = <0>;
+		pinctrl-names = "default";
+		pinctrl-0 = <&mcp251xfd0_pins>;
+		spi-max-frequency = <20000000>;
+		interrupt-parent = <&gpio>;
+		interrupts = <27 IRQ_TYPE_LEVEL_LOW>;
+		clocks = <&clk_mcp251xfd_osc>;
+	};
+
+	mcp251xfd1: mcp251xfd@1 {
+		compatible = "microchip,mcp251xfd";
+		reg = <1>;
+		pinctrl-names = "default";
+		pinctrl-0 = <&mcp251xfd1_pins>;
+		spi-max-frequency = <20000000>;
+		interrupt-parent = <&gpio>;
+		interrupts = <22 IRQ_TYPE_LEVEL_LOW>;
+		clocks = <&clk_mcp251xfd_osc>;
+	};
+};
+
+&gpio {
+	mcp251xfd0_pins: mcp251xfd0_pins {
+		brcm,pins = <27>;
+		brcm,function = <BCM2835_FSEL_GPIO_IN>;
+	};
+
+	mcp251xfd1_pins: mcp251xfd1_pins {
+		brcm,pins = <22>;
+		brcm,function = <BCM2835_FSEL_GPIO_IN>;
+	};
+};
+
+/* uart5 communicates with the STN2120 OBD controller */
+&uart5 {
+	pinctrl-names = "default";
+	pinctrl-0 = <&uart5_gpio12>;
+	status = "okay";
+};
+
+&vc4 {
+	status = "okay";
+};
+
+&vec {
+	status = "disabled";
+};
diff --git a/arch/arm64/boot/dts/broadcom/Makefile b/arch/arm64/boot/dts/broadcom/Makefile
index 7908ab2..2e08336 100644
--- a/arch/arm64/boot/dts/broadcom/Makefile
+++ b/arch/arm64/boot/dts/broadcom/Makefile
@@ -2,6 +2,7 @@
 dtb-$(CONFIG_ARCH_BCM2835) += bcm2711-rpi-400.dtb \
 			      bcm2711-rpi-4-b.dtb \
 			      bcm2711-rpi-4-b-7inch-ts-dsi.dtb \
+			      bcm2711-rpi-cm4-canopi.dtb \
 			      bcm2711-rpi-cm4-io.dtb \
 			      bcm2837-rpi-3-a-plus.dtb \
 			      bcm2837-rpi-3-b.dtb \
diff --git a/arch/arm64/boot/dts/broadcom/bcm2711-rpi-cm4-canopi.dts b/arch/arm64/boot/dts/broadcom/bcm2711-rpi-cm4-canopi.dts
new file mode 100644
index 0000000..e9369aa
--- /dev/null
+++ b/arch/arm64/boot/dts/broadcom/bcm2711-rpi-cm4-canopi.dts
@@ -0,0 +1,2 @@
+// SPDX-License-Identifier: GPL-2.0
+#include "arm/bcm2711-rpi-cm4-canopi.dts"
